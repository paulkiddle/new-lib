. $NVM_DIR/nvm.sh
for V in $(node ./build/version.cjs ls)
do
	nvm use $V
	./build/test.sh
done