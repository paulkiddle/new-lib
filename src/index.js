/**
 * New lib
 * @module new-lib
 * @see {@link module:new-lib.default}
 */

/**
 * @name module:new-lib.default
 */
export default () => 'Nothing here yet';
